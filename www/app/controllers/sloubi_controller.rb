require 'espeak'

class SloubiController < ApplicationController

	def index
		@espeak = Espeak.instance
		@espeak.set_voice_by_name 'en'
		wave_data = @espeak.synth "sloubi #{params[:id]}."
		render :text => wave_data.read, :content_type => 'audio/x-wav'
	end

end
