class GamesController < ApplicationController

	PLAYERS = [:karadoc, :perceval]

	before_filter :connect

  # GET /games
  # GET /games.xml
	def index
		list
	end

  # GET /games/list
  # GET /games/list.xml
	def list
		@games = @@games_server.list_available

    respond_to do |format|
      format.html # list.html.erb
      format.xml  { render :xml => @games }
    end
	end

  # POST /games/create
  # POST /games/create.xml
	def create
		creator = params['creator']
		player = params['player'].to_sym
		game = @@games_server.create creator, player
		id = @@games_server.list.key game
		@@clients_server.new_client id, player, game.uri

    respond_to do |format|
      format.html { redirect_to :controller => 'game', :id => id, :player => player }
      format.xml  { render :xml => game }
    end
	end

  # POST /games/join
  # POST /games/join.xml
	def join
		id = params['id'].to_i
		game = @@games_server.list[id]
		player = PLAYERS.find do |player|
		         	player != game.player
		         end
		@@games_server.join game
		@@clients_server.new_client id, player, game.uri

    respond_to do |format|
      format.html { redirect_to :controller => 'game', :id => id, :player => player }
      format.xml  { render :xml => game }
    end
	end

	protected

	def connect
		@@games_server ||= DRbObject.new_with_uri Rails.application.config.games_server_uri
		@@clients_server ||= DRbObject.new_with_uri Rails.application.config.clients_server_uri
	end

end
