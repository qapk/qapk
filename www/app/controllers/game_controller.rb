class GameController < ApplicationController

	around_filter :get_state

  # POST /game/action_ask
  # POST /game/action_ask.xml
	def action_ask
		action = @state.model.actions.at params['action_chosen'].to_i
		@game_server.answer @player, action

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => action }
    end
	end

  # POST /game/action_stay
  # POST /game/action_stay.xml
	def ask_stay
		stay = params.include? 'stay'
		@game_server.answer @player, stay

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => stay }
    end
	end

  # POST /game/action_place
  # POST /game/action_place.xml
	def ask_place
		index = params['place'].to_i.modulo @state.model.places.size
		place = @state.model.places.keys.at index
		@game_server.answer @player, place

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => place }
    end
	end

  # POST /game/action_riddle
  # POST /game/action_riddle.xml
	def ask_riddle
		answer = params['riddle'].strip.downcase
		answer[/^(a )?/] = ''
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/action_essay
  # POST /game/action_essay.xml
	def ask_essay
		answer = params['essay']
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/action_slaves_to_buy
  # POST /game/action_slaves_to_buy.xml
	def ask_slaves_to_buy
		answer = params['slaves_to_buy'].to_i
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/action_slaves_to_sell
  # POST /game/action_slaves_to_sell.xml
	def ask_slaves_to_sell
		answer = params['slaves_to_sell'].to_i
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/action_squares_to_dig
  # POST /game/action_squares_to_dig.xml
	def ask_squares_to_dig
		answer = if params['squares_to_dig'].nil?
		         	[]
		         else
		         	params['squares_to_dig'].collect do |square|
		         		(square.split ' ').collect do |square_coord|
		         			square_coord.to_i
		         		end
		         	end.uniq - @state.args[0]
		         end
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/ask_bottle
  # POST /game/ask_bottle.xml
	def ask_bottle
		initial_speed = (rand * 2 + 3) * 360
		initial_angle = 0.to_f
		answer = [initial_speed, initial_angle]
		@game_server.answer @player, answer

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/sloubi
  # POST /game/sloubi.xml
	def sloubi
		@game_server.answer @player, params['sloubi'].to_i

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => answer }
    end
	end

  # POST /game/end
  # POST /game/end.xml
	def end
		@game_server.answer @player
		@@clients_server.kill_client @id, @player

    respond_to do |format|
      format.html { redirect_to :controller => 'games' }
      format.xml  { render :xml => nil }
    end
	end

  # POST /game/nod
  # POST /game/nod.xml
	def nod
		@game_server.answer @player

    respond_to do |format|
      format.html { redirect_to :action => 'index' }
      format.xml  { render :xml => nil }
    end
	end

	protected

	def get_state
		@id = params['id'].to_i
		@player = params['player'].to_sym
		@@games_server ||= DRbObject.new_with_uri Rails.application.config.games_server_uri
		@@clients_server ||= DRbObject.new_with_uri Rails.application.config.clients_server_uri
		@@game_servers ||= Hash.new
		uri = @@games_server.list[@id].uri
		@@game_servers[@id] ||= DRbObject.new_with_uri uri
		@game_server = @@game_servers[@id]
		@game_server.state do |state|
			@state = state
			if not @state.players.has_key? @player or @game_server.result.has_key? @player
				render :action => 'refresh'
			else
				yield
			end
		end
	end

end
