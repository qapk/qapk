module ApplicationHelper

	def juggernaut_include_tag
		config = Rails.application.config.juggernaut
		script_url = "http://#{config[:host]}:#{config[:port]}/application.js"
		javascript_include_tag script_url
	end

	def juggernaut_js_options
		config = Rails.application.config.juggernaut
		"{host: '#{config[:host]}', port: #{config[:port]}}"
	end

end
