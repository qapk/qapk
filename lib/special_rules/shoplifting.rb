require 'board'
require 'character'
require 'perceval'
require 'karadoc'
require 'special_rules_base'

module QAPK
	module SpecialRules

		class Shoplifting < SpecialRulesBase

			def initialize *args
				super *args
				board.class.send :alias_method, :old_move, :move unless board.respond_to? :old_move
				shoplifting = self
				board.class.send :define_method, :move do |elt, old_index, new_index|
					shoplifting.run elt, old_index, new_index
					self.old_move elt, old_index, new_index
				end
			end

			def run elt, old_index, new_index
				if elt.kind_of? Character and old_index != new_index and (@board.index Perceval) == (@board.index Karadoc)
					nb_slaves = elt.slaves_real[0] + elt.slaves_real[1] / 2 + 1
					other_player = @board[*old_index].find do |elt_f|
						elt_f.kind_of? Character and elt_f != elt
					end
					slaves_healthy, slaves_ill = other_player.slaves_real
					if nb_slaves < slaves_ill
						slaves_healthy = 0
						slaves_ill = nb_slaves
					else
						slaves_healthy = [slaves_healthy, nb_slaves - slaves_ill].min
					end
					unless slaves_healthy == 0 and slaves_ill == 0
						other_player.slaves_withdraw slaves_healthy, slaves_ill
						elt.slaves_add slaves_healthy, slaves_ill
						view.show_slaves_taken elt, other_player, slaves_healthy, slaves_ill
						view.show_slaves_withdrawn other_player, elt, slaves_healthy, slaves_ill
					end
				end
			end

		end

	end
end
