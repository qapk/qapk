# encoding: utf-8

module QAPK

	class Dice < Object

		def self.roll
			return (rand 6) + 1
		end

	end

	class Bottle < Object

		def self.turn initial_speed, initial_angle
			time_to_stop = 8.to_f
			friction_constant = 1.to_f
			((initial_speed * (-1 / friction_constant * Math.exp(- friction_constant * time_to_stop) - 1) + initial_angle - initial_speed * (1 / friction_constant + 1)).modulo 360).round 1
		end

	end

end
