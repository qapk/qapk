# encoding: utf-8

require 'malus_base'

module QAPK
	module Malus

		class OldWomanTown < MalusBase

			NAME = 'the old woman town'

			def run player
				slaves_to_sicken = player.slaves_real[0]
				player.slaves_sicken slaves_to_sicken
				view.show_sickened player, slaves_to_sicken
			end

		end

	end
end
