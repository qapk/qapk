# encoding: utf-8

require 'malus_base'

module QAPK
	module Malus

		class SloubiSong < MalusBase

			NAME = 'the Sloubi song'

			def run player
				dice = -4
				20.times do
					dice += Dice.roll
				end
				sloubi = view.sloubi player, dice
				if sloubi != dice
					player.sloubicized!
					view.sloubicized player
				else
					view.not_sloubicized player
				end
			end

		end

	end
end
