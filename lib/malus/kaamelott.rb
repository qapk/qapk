# encoding: utf-8

require 'malus_base'

module QAPK
	module Malus

		class Kaamelott < MalusBase

			NAME = 'Kaamelott'

			def run player
				player.slaves = [0, 0]
				view.slaves_lost player
			end

		end

	end
end
