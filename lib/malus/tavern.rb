# encoding: utf-8

require 'malus_base'

module QAPK
	module Malus

		class Tavern < MalusBase

			NAME = 'the tavern'

			TURNS_TO_SKIP = 3

			def run player
				@game.skip_turns player, TURNS_TO_SKIP
				view.show_skipped player, TURNS_TO_SKIP
			end

		end

	end
end
