# encoding: utf-8

require 'malus_base'

module QAPK
	module Malus

		class EtherMedallion < MalusBase

			NAME = 'the Ether medallion'

			def run player
				player.money = [0, 0]
				view.transformed player
			end

		end

	end
end
