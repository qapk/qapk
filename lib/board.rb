# encoding: utf-8

require 'object'
require 'dice'

module QAPK

	class Square < Object

		def initialize
			@dug = false
			@elts = []
		end

		def dug?
			@dug
		end

		def dug!
			@dug = true
		end

		def method_missing name, *args, &block
			@elts.send name, *args, &block
		end

	end

	class Board < Object

		class IndexOutOfRangeException < Exception
		end

		class NotMoved < Exception
		end

		attr_reader :column_nb
		attr_reader :line_nb

		def initialize line_nb, column_nb
			@line_nb = line_nb
			@column_nb = column_nb
			@board = Array.new
			@line_nb.times do |line_index|
				@board.push Array.new
				@column_nb.times do
					@board[line_index].push Square.new
				end
			end
			@elements = Hash.new
			@elements_old = Hash.new
		end

		def each_line &block
			@board.each &block
		end

		def [] line_index, column_index
			@board[line_index - 1][column_index - 1]
		end

		def []= line_index, column_index, value
			if line_index == :rand or column_index == :rand
				line_index_tmp, column_index_tmp = line_index, column_index
				begin
					line_index = (rand @line_nb) + 1 if line_index_tmp == :rand
					column_index = (rand @column_nb) + 1 if column_index_tmp == :rand
				end while not self[line_index, column_index].empty?
			end
			@board[line_index - 1][column_index - 1].push value
			if @elements.has_key? value and @elements[value] != [line_index, column_index]
				p = path *@elements[value], line_index, column_index
				@elements_old[value] = if p.size >= 2
				                       	p[-2]
				                       else
				                       	@elements[value]
				                       end
			end
			@elements[value] = [line_index, column_index]
		end

		def move elt, old_index, new_index
			self[*old_index].delete elt
			self[*new_index] = elt
		end

		def index elt
			if @elements.has_key? elt
				@elements[elt]
			else
				@elements.find do |elt_test, value|
					elt_test.kind_of? elt
				end[1]
			end
		end

		def path old_line_index, old_column_index, new_line_index, new_column_index
			path = []
			while old_line_index != new_line_index or old_column_index != new_column_index
				axis = if (new_line_index - old_line_index).abs > (new_column_index - old_column_index).abs
				       	:horizontal
				       elsif (new_line_index - old_line_index).abs < (new_column_index - old_column_index).abs
				       	:vertical
				       else
				       	dice = Dice.roll
				       	if [1, 2, 4].include? dice
				       		:horizontal
				       	elsif [3, 5, 6].include? dice
				       		:vertical
				       	end
				       end
				if axis == :horizontal
					old_line_index += (new_line_index - old_line_index).sign
				elsif axis == :vertical
					old_column_index += (new_column_index - old_column_index).sign
				end
				path.push [old_line_index, old_column_index]
			end
			path
		end

		def direction elt
			raise NotMoved if not @elements_old.has_key? elt
			@elements[elt].collect.with_index do |pos, index|
				pos - @elements_old[elt][index]
			end
		end

		def line_index_add line_index, n
			raise IndexOutOfRangeException if line_index + n > @line_nb or line_index + n < 1
			line_index + n
		end

		def column_index_add column_index, n
			raise IndexOutOfRangeException if column_index + n > @column_nb or column_index + n < 1
			column_index + n
		end

		def index_add index, n
			[(line_index_add index[0], n[0]), (column_index_add index[1], n[1])]
		end

		def distance index1, index2
			[index1, index2].transpose.collect do |index|
				(index[0] - index[1]).abs
			end.max
		end

	end

end
