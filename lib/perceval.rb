# encoding: utf-8

require 'character'

module QAPK

	class Perceval < Character

		NAME = 'Perceval'

		alias :money :money_real
		alias :slaves :slaves_real

	end

end
