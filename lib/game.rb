# encoding: utf-8

require 'object'
require 'board'
require 'bonus'
require 'malus'
require 'graal'
require 'action'
require 'perceval'
require 'karadoc'
require 'dice'
require 'needs_view'
require 'special_rules'

module QAPK

	class WeekDay < Object

		DAYS = :monday, :tuesday, :wednesday, :thursday, :friday, :saturday, :sunday

		def initialize day
			@day = DAYS.index day
		end

		def + nb
			WeekDay.new DAYS[(@day + nb).modulo DAYS.size]
		end

		def next
			@day = (@day + 1).modulo DAYS.size
		end

		def to_s
			DAYS[@day].to_s
		end

		def == day
			if DAYS.include? day
				DAYS[@day] == day
			else
				super
			end
		end

	end

	class Game < Object
		include NeedsView

		COLUMN_NB = 6
		LINE_NB = 6

		attr_reader :actions, :board, :week_day

		def initialize view_factory
			super view_factory, self, nil, nil
			@week_day = WeekDay.new :monday
			@perceval = Perceval.new @week_day, 2, 1
			@karadoc = Karadoc.new @week_day, 2, 1
			karadoc_dice = Dice.roll * Math::PI / 3
			perceval_dice = Dice.roll * Math::PI / 3
			@players = if (karadoc_dice - (1 + Math.sqrt(5)) / 2).abs < (perceval_dice - (1 + Math.sqrt(5)) / 2).abs
			             [@karadoc, @perceval]
			           else
			             [@perceval, @karadoc]
			           end
			@turns_to_skip = Hash.new 0
			@board = Board.new LINE_NB, COLUMN_NB
			[Bonus, Malus].each do |mod|
				mod.constants.each do |mod_constant|
					mod_class = mod.const_get mod_constant
					@board[:rand, :rand] = mod_class if mod_class.kind_of? Class
				end
			end
			@board[:rand, :rand] = Graal.new
			kaamelott_pos = @board.index Malus::Kaamelott
			@board[*kaamelott_pos] = @perceval
			@board[*kaamelott_pos] = @karadoc
			actions = Action.constants.collect do |action_constant|
				Action.const_get action_constant
			end.delete_if do |action_class|
				not action_class.kind_of? Class
			end
			@actions = []
			until actions.empty?
				action = actions.find do |action|
					action.after? @actions.last
				end
				@actions.push action
				actions.delete action
			end
			@winner = nil
			SpecialRules.constants.collect do |special_rules_constant|
				special_rules_class = SpecialRules.const_get special_rules_constant
				instanciate_viewable special_rules_class if special_rules_class.kind_of? Class
			end
		end

		def run
			while not turn
			end
			view.winner @players, @winner unless @winner.nil?
		end

		def win player
			@winner = player
		end

		def lost player
			other_player = @players.find do |other_player|
				other_player != player
			end
			win other_player
		end

		def skip_turn player
			skip_turns player, 1
		end

		def skip_turns player, turns_to_skip
			@turns_to_skip[player] += turns_to_skip
		end

		def turn
			@players.each do |player|
				if @turns_to_skip[player] > 0
					@turns_to_skip[player] -= 1
				else
					should_exit = turn_player player
					return true if should_exit
				end
			end
			@week_day.next
			false
		end

		def turn_player player
			player.silenced -= 1 if player.silenced > -1
			okay = false
			until okay
				action_klass = view.action_ask player
				return true if action_klass.nil?
				action = instanciate_viewable action_klass
				if not action.runable? player
					action.view.not_runable player
				else
					action.run player
					okay = true
				end
			end
			new_pos = @board.index player
			@board[*new_pos].each do |o|
				o = instanciate_viewable o if o.kind_of? Class
				o.run player if o.respond_to? :run and action.should_run o
			end
			not @winner.nil?
		end

		def instanciate_viewable klass
			klass.new @view_factory, self, @board, @players
		end

	end

end
