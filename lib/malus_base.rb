# encoding: utf-8

require 'object'
require 'needs_view'
require 'has_name'

module QAPK

	class MalusBase < Object
		include NeedsView
		include HasName
		extend HasName
	end

end
