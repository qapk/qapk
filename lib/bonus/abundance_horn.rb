# encoding: utf-8

require 'bonus_base'

module QAPK
	module Bonus

		class AbundanceHorn < BonusBase

			ONION = 4
			BREAD = 3

			NAME = 'the abundance horn'

			def run player
				view.given player, BREAD, ONION
				player.money_add BREAD, ONION
			end

		end

	end
end
