# encoding: utf-8

require 'bonus_base'

module QAPK
	module Bonus

		class VenneksBarn < BonusBase

			SLAVES = 10

			NAME = 'Vennek\'s barn'

			def run player
				player.slaves_add 0, SLAVES
				view.print_slaves_taken player, SLAVES
			end

		end

	end
end
