# encoding: utf-8

require 'bonus_base'

module QAPK
	module Bonus

		class LakeWoman < BonusBase

			NAME = 'the lake woman'

			def run player
				slaves_to_heal = player.slaves_real[1]
				player.slaves_heal slaves_to_heal
				view.show_healed player, slaves_to_heal
			end

		end

	end
end
