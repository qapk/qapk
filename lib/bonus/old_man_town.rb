# encoding: utf-8

require 'bonus_base'
require 'graal'

module QAPK
	module Bonus

		class OldManTown < BonusBase

			NAME = 'the old man town'

			def run player
				index = @board.index Graal
				begin
					line_rand = (rand @board.line_nb) + 1
					column_rand = (rand @board.column_nb) + 1
				end while (@board.distance index, [line_rand, column_rand]) > 2
				view.show_pos player, line_rand, column_rand
			end

		end

	end
end
