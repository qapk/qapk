# encoding: utf-8

require 'bonus_base'

module QAPK
	module Bonus

		class OldManRiddle < BonusBase

			NAME = 'the old man riddle'

			def run player
				answer = view.ask_riddle player
				if answer == 'brown'
					view.print_good_answer player
					other_player = @players.find do |other_player|
						other_player != player
					end
					@game.skip_turn other_player
				else
					view.print_bad_answer player
				end
			end

		end

	end
end
