# encoding: utf-8

module QAPK

	module NeedsView

		attr_reader :game, :board, :players

		def initialize view_factory, game, board, players, *args
			@view_factory = view_factory
			@game = game
			@board = board
			@players = players
		end

		def view
			@view_factory.view self
		end

	end

end
