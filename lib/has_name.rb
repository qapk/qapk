# encoding: utf-8

module QAPK

	module HasName

		def human_name
			self.game_class.const_get 'NAME'
		end

	end

end
