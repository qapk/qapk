# encoding: utf-8

class String

	def camelize
		self.gsub(/\/(.?)/) { "::" + $1.upcase }.gsub(/(^|_)(.)/) { $2.upcase }
	end

	def underscore
		gsub(/::/, '/').gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').gsub(/([a-z\d])([A-Z])/,'\1_\2').tr("-", "_").downcase
	end

end

module Kernel

	# http://en.wikipedia.org/wiki/Box_Muller_transform
	def gaussian_rand
		u1 = 1 - rand # For Box-Muller transform, the interval is (0, 1], but the rand interval is [0, 1)
		u2 = 1 - rand
		(Math.sqrt -2 * (Math.log u1)) * (Math.cos 2 * Math::PI * u2)
	end

end

class Integer

	def sign
		if self < 0
			-1
		elsif self > 0
			1
		else
			0
		end
	end

end
