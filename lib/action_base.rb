# encoding: utf-8

require 'object'
require 'needs_view'
require 'has_name'

module QAPK

	class ActionBase < Object
		include NeedsView
		include HasName
		extend HasName

		def runable? player
			true
		end

		def run player
		end

		def should_run bonus_malus
			false
		end

		def self.after? action
			action_before == action.class or action_before == action
		end

	end

end
