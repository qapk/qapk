# encoding: utf-8

require 'object'
require 'has_name'

module QAPK

	class Character < Object
		include HasName

		class NotEnoughMoney < Exception
		end

		class NotEnoughSlaves < Exception
		end

		ONION_IN_BREAD = 5

		attr_writer :week_day
		attr_accessor :silenced

		def initialize week_day, onion, bread
			@week_day = week_day
			@onion = 0
			@bread = 0
			money_add onion, bread
			@slaves_healthy = 0
			@slaves_ill = 0
			@silenced = -1
			@sloubicized = false
		end
		
		def name
			(self.class.name.split '::').last
		end

		def to_sym
			name.downcase.to_sym
		end

		def week_day
			:unknown
		end

		def week_day_real
			@week_day
		end

		def money
			[:unknown, :unknown]
		end

		def money_real
			[@bread, @onion]
		end
	
		def slaves
			[:unknown, :unknown]
		end

		def slaves_real
			[@slaves_healthy, @slaves_ill]
		end

		def money= args
			bread, onion = *args
			@bread = 0
			@onion = 0
			money_add bread, onion
		end

		def money_add bread, onion
			@onion += onion
			@bread += bread + @onion / ONION_IN_BREAD
			@onion = @onion.modulo ONION_IN_BREAD
		end

		def money_withdraw bread, onion
			bread += onion / ONION_IN_BREAD
			onion = onion.modulo ONION_IN_BREAD
			if onion > @onion
				bread += 1
				onion -= ONION_IN_BREAD
			end
			raise NotEnoughMoney.new if bread > @bread
			@bread -= bread
			@onion -= onion
		end

		def slaves= args
			healthy, ill = *args
			@slaves_healthy = 0
			@slaves_ill = 0
			slaves_add healthy, ill
		end

		def slaves_add healthy, ill
			@slaves_healthy += healthy
			@slaves_ill += ill
		end

		def slaves_withdraw healthy, ill
			raise NotEnoughSlaves.new if healthy > @slaves_healthy or ill > @slaves_ill
			@slaves_healthy -= healthy
			@slaves_ill -= ill
		end

		def slaves_heal slaves_nb
			@slaves_ill -= slaves_nb
			@slaves_healthy += slaves_nb
		end

		def slaves_sicken slaves_nb
			@slaves_ill += slaves_nb
			@slaves_healthy -= slaves_nb
		end

		def sloubicized!
			@sloubicized = true
		end

		def unsloubicized
			@sloubicized = false
		end

		def sloubicized?
			@sloubicized
		end

	end

end
