# encoding: utf-8

require 'action_base'
require 'dice'
require 'graal'

module QAPK
	module Action

		class Move < ActionBase

			NAME = 'move'

			DIRECTION = {2 => :north, 3 => :south, 4 => :east, 5 => :west}
			NORTH = {2 => :front, 3 => :behind, 4 => :left, 5 => :right}
			ANGLE_TO_DIRECTION = {[0..45, 315..360] => :right, 45..135 => :top, 135..225 => :left, 225..315 => :bottom}
			DIRECTION_TO_NORTH = {:top => :front, :bottom => :behind, :left => :left, :right => :right}
			DIRECTION_TO_COORDS = {:top => [-1, 0], :bottom => [1, 0], :left => [0, -1], :right => [0, 1]}

			def self.action_before
				NilClass
			end

			def initialize *args
				super *args
				@should_run_bonus_malus = false
			end

			def should_run bonus_malus
				@should_run_bonus_malus
			end

			@@last_moves = Hash.new

			def self.last_move player
				@@last_moves[player]
			end

			def run player
				square_nb = Dice.roll
				view.square_nb player, square_nb
				direction = Dice.roll
				view.direction player, direction
				north = Dice.roll
				view.north player, north
				view.move_towards_graal player if direction == 6
				graal_index = @board.index Graal
				player_index = @board.index player
				square_direction = nil
				next_square_directions = []
				square_north = nil
				next_square_norths = []
				player_direction = begin
				                   	@board.direction player
				                   rescue Board::NotMoved
				                   	view.bottle_first_move player
				                   	angle = Bottle.turn *(view.ask_bottle player)
				                   	view.bottle_angle player, angle
				                   	player_direction = direction_from_angle((angle + 90).modulo 360)
				                   	DIRECTION_TO_COORDS[player_direction]
				                   end
				view.player_direction player, (DIRECTION_TO_COORDS.key player_direction)
				move = []
				if north == 6
					view.bottle_north player
					angle = Bottle.turn *(view.ask_bottle player)
					view.bottle_angle player, angle
					north_direction = direction_from_angle((angle - 90).modulo 360)
					north = NORTH.key DIRECTION_TO_NORTH[north_direction]
					view.north player, north
				end
				square_nb.times do |step|
					square_direction = dice_special_rules player, direction, square_nb - step - 1, square_direction, next_square_directions
					square_north = dice_special_rules player, north, square_nb - step - 1, square_north, next_square_norths
					if square_direction == 1 or square_north == 1
						view.dice_is_one_move_over player
						break
					end
					if direction == 1 and square_direction == 6
						view.dice_is_six_move_over player
						break
					end
					if square_direction == 6 and player_index != graal_index
						path = @board.path *player_index, *graal_index
						new_player_index = path.first
						new_player_index.collect!.with_index do |value, index|
							player_index[index] - value
						end if player.sloubicized?
					else
						if direction == 6
							square_direction = DIRECTION.key :north
							square_north = NORTH.key :front
						end
						view.direction player, square_direction if square_direction != direction
						view.north player, square_north if square_north != north
						new_player_direction = player_direction.clone
						if [:left, :right].include? NORTH[square_north]
							new_player_direction[1] = new_player_direction[1] * -1
							new_player_direction.reverse!
						end
						if [:behind, :right].include? NORTH[square_north]
							new_player_direction.collect! do |elt|
								elt * -1
							end
						end
						if [:west, :east].include? DIRECTION[square_direction]
							new_player_direction[1] = new_player_direction[1] * -1
							new_player_direction.reverse!
						end
						if [:south, :east].include? DIRECTION[square_direction]
							new_player_direction.collect! do |elt|
								elt * -1
							end
						end
						begin
							new_player_index = @board.index_add player_index, new_player_direction
						rescue Board::IndexOutOfRangeException
							new_player_index = player_index
						end
					end
					@board.move player, player_index, new_player_index
					if player_index != new_player_index
						@should_run_bonus_malus = true
						step = new_player_index.collect.with_index do |value, index|
							value - player_index[index]
						end
						move.push step
					end
					player_index = new_player_index
				end
				@@last_moves[player] = move
			end

			private

			def direction_from_angle angle
				ANGLE_TO_DIRECTION.find do |ranges, direction|
					ranges = [ranges] if ranges.kind_of? Range
					ranges.find do |range|
						range === angle
					end
				end[1]
			end

			def dice_special_rules player, value, max_reroll, square_value, next_square_values
				if value == 1
					view.dice_is_one player
					if next_square_values.size > 0
						square_value = next_square_values.pop
						view.using_saved_dice player, square_value
					else
						old_square_value = square_value
						square_value = Dice.roll
						view.dice_rerolled player, square_value
						if square_value == 6
							if old_square_value.nil?
								nb_roll = 0
								while not (1..5).include? square_value and nb_roll < max_reroll
									view.dice_is_one_no_previous_dice player
									square_value = Dice.roll
									view.dice_rerolled player, square_value
									nb_roll += 1
								end
								unless square_value == 6
									view.saved_rolls player, nb_roll
									next_square_values += [square_value] * nb_roll
								end
							else
								square_value = old_square_value
								view.using_previous_dice player, square_value
							end
						end
					end
				else
					square_value = value
				end
				square_value
			end

		end

	end
end
