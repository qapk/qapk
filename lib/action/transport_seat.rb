# encoding: utf-8

require 'action_base'

module QAPK
	module Action

		class TransportSeat < ActionBase

			NAME = 'use the transport seat'

			def self.action_before
				Doublette
			end

			def run player
				other_player = @players.find do |other_player|
					other_player != player
				end
				player_index = @board.index player
				other_player_index = @board.index other_player
				@board.move player, player_index, other_player_index
			end

			def should_run bonus_malus
				true
			end

		end

	end
end
