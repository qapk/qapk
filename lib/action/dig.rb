# encoding: utf-8

require 'action_base'
require 'graal'
require 'misc'
require 'board'

module QAPK
	module Action

		class Dig < ActionBase

			NAME = 'dig'

			def self.action_before
				Sell
			end

			def run player
				nb_squares = player.slaves_real[0] +  player.slaves_real[1] / 2 + 1

				distance_max = -1
				squares = []
				player_index = @board.index player
				begin
					old_squares = squares
					distance_max += 1
					squares = []
					(-distance_max).upto distance_max do |row_delta|
						(-distance_max).upto distance_max do |col_delta|
							begin
								square = @board.index_add player_index, [row_delta, col_delta]
								squares.push square
							rescue Board::IndexOutOfRangeException
							end
						end
					end
				end while squares.size <= nb_squares
				squares = old_squares
				nb_squares -= squares.size

				squares_ok = false
				begin
					squares_selected = view.ask_squares_to_dig player, squares, nb_squares, distance_max
					not_ok_squares = squares_selected.find_all do |square|
						(@board.distance square, player_index) > distance_max
					end
					squares_ok = if squares_selected.size > nb_squares
					             	view.too_many_squares player, nb_squares, squares_selected.size
					             	false
					             elsif not not_ok_squares.empty?
					             	view.squares_too_far player, distance_max, not_ok_squares if not squares_ok
					             	false
					             else
					             	true
					             end
				end until squares_ok
				squares += squares_selected

				nb_squares_not_dug = squares.count do |square|
					not @board[*square].dug?
				end
				nb_onions = (gaussian_rand.abs * 3 * nb_squares_not_dug).to_i
				player.money_add nb_onions, 0
				view.print_onions_found player, nb_onions

				squares.each do |square|
					@board[*square].dug!
				end

				graal_index = @board.index Graal
				if not (squares.index graal_index).nil?
					view.print_graal_found player, graal_index
					@game.win player 
				end
			end

		end

	end
end
