# encoding: utf-8

require 'action_base'

module QAPK
	module Action

		class BreadDumplings < ActionBase

			NAME = 'stick bread dumplings into my nose'

			def self.action_before
				ChangeOrder
			end

		end

	end
end
