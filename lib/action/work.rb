# encoding: utf-8

require 'action_base'
require 'malus'
require 'character'

module QAPK
	module Action

		class Work < ActionBase

			NAME = 'work at the tavern'

			def self.action_before
				TransportSeat
			end

			EARN = 4 + 1 * Character::ONION_IN_BREAD

			def run player
				earn = EARN.to_f
				tavern_index = @board.index Malus::Tavern
				player_index = @board.index player
				if tavern_index == player_index
					earn += earn / 6
				else
					if view.ask_stay_tavern player
						@board.move player, player_index, tavern_index
					else
						earn -= earn / 6
					end
				end
				player.money_add 0, earn.to_i
			end

		end

	end
end
