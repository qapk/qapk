# encoding: utf-8

require 'action_base'
require 'dice'
require 'graal'

module QAPK
	module Action

		class Doublette < ActionBase

			NAME = 'doublette'

			def self.action_before
				Move
			end

			def initialize *args
				super *args
				@should_run_bonus_malus = false
			end

			def should_run bonus_malus
				@should_run_bonus_malus
			end

			def runable? player
				other_player = @players.find do |other_player|
					other_player != player
				end
				move = Move.last_move other_player
				not move.nil?
			end

			def run player
				player_index = @board.index player
				other_player = @players.find do |other_player|
					other_player != player
				end
				player_index = @board.index player
				move = Move.last_move other_player
				move.each do |step|
					begin
						new_player_index = @board.index_add player_index, step
					rescue Board::IndexOutOfRangeException
						new_player_index = player_index
						end
					@board.move player, player_index, new_player_index
					@should_run_bonus_malus = true if player_index != new_player_index
					player_index = new_player_index
				end
			end

		end

	end
end
