# encoding: utf-8

require 'action_base'
require 'malus'

module QAPK
	module Action

		class Merlin < ActionBase

			NAME = 'go and see Merlin'

			HEALED_SLAVES = 7

			def self.action_before
				Dig
			end

			def run player
				healed_slaves = HEALED_SLAVES.to_f
				kaamelott_index = @board.index Malus::Kaamelott
				player_index = @board.index player
				if kaamelott_index == player_index
					healed_slaves += healed_slaves / 6
				else
					if view.ask_stay_kaamelott player
						@board.move player, player_index, kaamelott_index
					else
						healed_slaves -= healed_slaves / 6
					end
				end
				healed_slaves = [healed_slaves.to_i, player.slaves_real[1]].min
				player.slaves_heal healed_slaves
			end

		end

	end
end
