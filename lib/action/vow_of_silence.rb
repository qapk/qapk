# encoding: utf-8

require 'action_base'

module QAPK
	module Action

		class VowOfSilence < ActionBase

			NAME = 'Take a vow of silence'

			SILENCED_TURNS = 5

			def self.action_before
				BreadDumplings
			end

			def run player
				player.silenced = SILENCED_TURNS
			end

		end

	end
end
