# encoding: utf-8

require 'action_base'
require 'action/buy'

module QAPK
	module Action

		class Sell < ActionBase

			NAME = 'sell slaves'

			SLAVE_PRICE = Buy::SLAVE_PRICE.collect do |price|
				price / 2
			end

			def self.action_before
				Buy
			end

			def run player
				slaves_to_sell = view.ask_slaves_to_sell player
				slave_price = SLAVE_PRICE
				if @game.week_day == :wenesday
					slave_price = slave_price.collect do |price|
						price * 2
					end
				elsif @game.week_day == :sunday
					slave_price = slave_price.collect do |price|
						price / 2
					end
				end
				slaves_price = slave_price.collect do |price|
					price * slaves_to_sell
				end
				begin
					slaves = [0, 0]
					slaves_to_sell.times do
						if player.slaves_real[0] == 0
							slaves[1] += 1
						elsif player.slaves_real[1] == 0
							slaves[0] += 1
						else
							slaves[(Dice.roll.even? and 0 or 1)] += 1
						end
					end
					player.slaves_withdraw *slaves
					player.money_add *slaves_price
					view.print_slaves_sold player, *slaves
				rescue Character::NotEnoughSlaves
					slaves_price = slaves_price.collect.with_index do |price, index|
						[price / 2, player.money_real[index]].min
					end
					view.print_not_enough_slaves player, slaves_price
					player.money_withdraw *slaves_price
				end
			end

		end

	end
end
