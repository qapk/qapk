# encoding: utf-8

require 'action_base'

begin
	require 'rubygems'
rescue LoadError
end
require 'pony'

module QAPK
	module Action

		class GiveUp < ActionBase

			NAME = 'give up'

			TO_ADDRESS = 'giga@le-pec.org'
			FROM_ADDRESS = 'giga@le-pec.org'
			SUBJECT = 'QAPK: give up essay'

			MAIL_METHOD = :sendmail
			VIA_OPTIONS = {:address => 'smtp.example.com', :port => 25, :user_name => 'user', :password => 'pass', :authentication => :plain, :domain => 'example.com'}

			def self.action_before
				DoNothing
			end

			def run player
				essay = view.ask_essay player
				Pony.mail :to => TO_ADDRESS, :from => FROM_ADDRESS, :subject => SUBJECT, :body => essay, :via => MAIL_METHOD, :via_options => VIA_OPTIONS
				@game.lost player
			end

		end

	end
end
