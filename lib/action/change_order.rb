# encoding: utf-8

require 'action_base'

module QAPK
	module Action

		class ChangeOrder < ActionBase

			NAME = 'change player order'

			def self.action_before
				GoTo
			end

		end

	end
end
