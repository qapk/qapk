# encoding: utf-8

require 'action_base'
require 'character'
require 'dice'

module QAPK
	module Action

		class Buy < ActionBase

			NAME = 'buy slaves'

			SLAVE_PRICE = [2, 4]

			def self.action_before
				Work
			end

			def run player
				slaves_to_buy = view.ask_slaves_to_buy player
				slave_price = SLAVE_PRICE
				if @game.week_day == :wednesday
					slave_price = slave_price.collect do |price|
						price / 2
					end
				elsif @game.week_day == :sunday
					slave_price = slave_price.collect do |price|
						price * 2
					end
				end
				slaves_price = slave_price.collect do |price|
					price * slaves_to_buy
				end
				begin
					player.money_withdraw *slaves_price
					slaves = [0, 0]
					slaves_to_buy.times do
						slaves[(Dice.roll.even? and 0 or 1)] += 1
					end
					player.slaves_add *slaves
					view.print_slaves_bought player, *slaves
				rescue Character::NotEnoughMoney
					slaves_price = slaves_price.collect do |price|
						price / 2
					end
					begin
						player.money_withdraw *slaves_price
					rescue Character::NotEnoughMoney
						slaves_price = player.money_real
						player.money_withdraw *slaves_price
					end
					view.print_not_enough_money player, slaves_price
				end
			end

		end

	end
end
