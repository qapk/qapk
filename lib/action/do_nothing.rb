# encoding: utf-8

require 'action_base'

module QAPK
	module Action

		class DoNothing < ActionBase

			NAME = 'do nothing'

			def self.action_before
				VowOfSilence
			end

		end

	end
end
