# encoding: utf-8

require 'action_base'
require 'malus'

module QAPK
	module Action

		class GoTo < ActionBase

			NAME = 'go to'

			def self.action_before
				Merlin
			end

			attr_reader :places

			def run player
				@places = Hash.new
				[Malus::Kaamelott, Malus::Tavern].each do |place|
					@places[@game.instanciate_viewable place] = @board.index place
				end
				place = view.ask_place player
				place_index = @places[place]
				player_index = @board.index player
				@board.move player, player_index, place_index
			end

		end

	end
end
