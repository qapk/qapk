# encoding: utf-8

require 'interface/object'
require 'interface/view'

module QAPK
	module Interface

		class Board < Object
			include View

			def draw
				print '-' * (5 * model.column_nb + 1)+ "\n"
				model.each_line do |line|
					print '|'
					line.each do |elts|
						print (elts.dug? and 'X' or ' ')
						elts = elts.find_all do |elt|
							has_view? elt
						end
						elts.each do |elt|
							(view elt).draw
						end
						print ' ' * (3 - elts.size)
						print '|'
					end
					print "\n" + '-' * (5 * model.column_nb + 1)+ "\n"
				end
			end

		end

	end
end
