# encoding: utf-8

require 'interface/text/character'

module QAPK
	module Interface

		class Karadoc < Character

			def draw
				print 'K'
			end

		end

	end
end
