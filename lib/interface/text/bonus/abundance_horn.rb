# encoding: utf-8

require 'interface/text/bonus_base'

module QAPK
	module Interface
		module Bonus

			class AbundanceHorn < BonusBase

				def draw
					print 'H'
				end

				def given player, bread, onion
					puts "Congratulation! You just received #{bread} piece of bread and #{onion} onions from the abundance horn!"
				end

			end

		end
	end
end
