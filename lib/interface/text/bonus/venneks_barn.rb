# encoding: utf-8

require 'interface/text/bonus_base'

module QAPK
	module Interface
		module Bonus

			class VenneksBarn < BonusBase

				def draw
					print 'V'
				end

				def print_slaves_taken player, slaves
					puts "You took #{slaves} ill slaves from Vennek's barn while he was out."
				end

			end

		end
	end
end
