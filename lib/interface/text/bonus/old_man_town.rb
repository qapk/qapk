# encoding: utf-8

require 'interface/text/bonus_base'

module QAPK
	module Interface
		module Bonus

			class OldManTown < BonusBase

				def draw
					print 'M'
				end

				def show_pos player, row, column
					puts "Here is a tip from the old woman pointed by the old man of the empty town: “the Grail is around #{row}, #{column}”."
				end

			end

		end
	end
end
