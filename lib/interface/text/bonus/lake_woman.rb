# encoding: utf-8

require 'interface/text/bonus_base'

module QAPK
	module Interface
		module Bonus

			class LakeWoman < BonusBase

				def draw
					print 'L'
				end

				def show_healed player, slaves_healed
					puts "The Lake Woman healed all your #{slaves_healed} ill slaves!"
				end

			end

		end
	end
end
