# encoding: utf-8

require 'interface/text/bonus_base'

module QAPK
	module Interface
		module Bonus

			class OldManRiddle < BonusBase

				def draw
					print 'R'
				end

				def ask_riddle player
					puts "What is tiny and brown?"
					begin
						answer = $stdin.readline.strip.downcase
						answer[/^(a )?/] = ''
						answer
					rescue EOFError
						nil
					end
				end

				def print_good_answer player
					puts "Correct! You can play again!"
				end

				def print_bad_answer player
					puts "Wrong!"
				end

			end

		end
	end
end
