# encoding: utf-8

require 'espeak'
require 'sdl'

require 'interface/text/malus_base'

module QAPK
	module Interface
		module Malus

			class SloubiSong < MalusBase

				SDL.init SDL::INIT_AUDIO
				SDL::Mixer.open

				def draw
					print 'S'
				end

				def sloubi player, dice
					@espeak = Espeak.instance
					@espeak.set_voice_by_name 'en'
					puts "The sloubi game is on!"
					puts "Your sloubi score is #{dice}."
					thread = Thread.new do
						Thread.current[:done] = false
						116.times do |i|
							Thread.current[:sloubi] = i + 1
							wave_data = @espeak.synth "sloubi #{i + 1}"
							wave = SDL::Mixer::Wave.load_from_io wave_data
							channel = SDL::Mixer.play_channel -1, wave, 0
							sleep 0.5 while SDL::Mixer.play? channel
							wave.destroy
							break if Thread.current[:done]
						end
					end
					sloubi = nil
					while sloubi.nil?
						chosen = $stdin.readline.strip
						if chosen == 'sloubi'
							sloubi = thread[:sloubi]
							thread[:done] = true
						end
					end
					thread.join
					puts "You sloubied at #{sloubi}."
					sloubi
				end

				def not_sloubicized player
					puts 'You won the sloubi game! You\'ve won the right to not lose anything.'
				end

				def sloubicized player
					puts 'You lost the sloubi game and have just been sloubicized!'
				end

			end

		end
	end
end
