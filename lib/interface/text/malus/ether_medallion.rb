# encoding: utf-8

require 'interface/text/malus_base'

module QAPK
	module Interface
		module Malus

			class EtherMedallion < MalusBase

				def draw
					print 'E'
				end

				def transformed player
					puts 'The Ether Medallion transformed all your objects into gaz! You have no breads nor onions left.'
				end

			end

		end
	end
end
