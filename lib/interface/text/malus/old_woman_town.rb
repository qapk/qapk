# encoding: utf-8

require 'interface/text/malus_base'

module QAPK
	module Interface
		module Malus

			class OldWomanTown < MalusBase

				def draw
					print 'W'
				end

				def show_sickened player, slaves_sickened
					puts "In this old town, you meet an old man who directs you to an old woman. This old woman sickens all your #{slaves_sickened} healthy slaves!"
				end

			end

		end
	end
end
