# encoding: utf-8

require 'interface/text/malus_base'

module QAPK
	module Interface
		module Malus

			class Tavern < MalusBase

				def draw
					print 'T'
				end

				def show_skipped player, turns_skipped
					puts "You've just arrived at the tavern. You stop by to eat something, take a room, and stay #{turns_skipped} turns."
				end

			end

		end
	end
end
