# encoding: utf-8

require 'interface/text/malus_base'

module QAPK
	module Interface
		module Malus

			class Kaamelott < MalusBase

				def draw
					print 'A'
				end

				def slaves_lost player
					puts 'You meet sir Arthur inside Kaamelott. He explains to you that knights are not allowed to use slaves when seeking the Holy Grail, and frees all your slaves!'
				end

			end

		end
	end
end
