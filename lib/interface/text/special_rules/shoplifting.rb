# encoding: utf-8

require 'interface/text/special_rules_base'

module QAPK
	module Interface
		module SpecialRules

			class Shoplifting < SpecialRulesBase

				def show_slaves_taken player, other_player, slaves_healthy, slaves_ill
					puts "You took #{slaves_healthy} healthy slaves and #{slaves_ill} ill slaves from #{other_player.human_name} by shoplifting him while going away."
				end

				def show_slaves_withdrawn player, other_player, slaves_healthy, slaves_ill
					puts "#{other_player.human_name} took #{slaves_healthy} healthy slaves and #{slaves_ill} ill slaves from you by shoplifting you while going away."
				end

			end

		end
	end
end
