# encoding: utf-8

require 'interface/object'
require 'interface/view'
require 'interface/text/has_name'

module QAPK
	module Interface

		class ActionBase < Object
			include View
			include HasName
		end

	end
end
