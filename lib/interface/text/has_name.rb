# encoding: utf-8

module QAPK
	module Interface

		module HasName

			def print_name
				print model.human_name.capitalize
			end

		end

	end
end

