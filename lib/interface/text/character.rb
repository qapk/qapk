# encoding: utf-8

require 'interface/object'
require 'interface/view'

module QAPK
	module Interface

		class Character < Object
			include View
		end

	end
end
