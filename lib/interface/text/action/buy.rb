# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Buy < ActionBase

				def ask_slaves_to_buy player
					slaves_to_buy = nil
					while slaves_to_buy.nil?
						puts 'How many slaves do you want to buy?'
						slaves_to_buy = /^(\d+)$/.match $stdin.readline.strip do |m|
							m[1].to_i
						end
					end
					slaves_to_buy
				end

				def print_not_enough_money player, slaves_price
					puts "You do not have enough money to buy this many slaves, thus you have to pay a penalty of #{slaves_price[0]} piece of bread and #{slaves_price[1]} onions."
				end

				def print_slaves_bought player, slaves_healthy, slaves_ill
					puts "You bought #{slaves_healthy} healthy slaves and #{slaves_ill} ill slaves."
				end

			end

		end
	end
end
