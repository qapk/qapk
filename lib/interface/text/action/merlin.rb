# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Merlin < ActionBase

				def ask_stay_kaamelott player
					stay = nil
					while stay.nil?
						puts 'Do you want to stay in Kaamelott after seeing Merlin? [y/n]'
						chosen = $stdin.readline.strip
						if /^y|n$/ =~ chosen
							stay = chosen == 'y'
						end
					end
					stay
				end

			end

		end
	end
end
