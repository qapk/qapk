# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Work < ActionBase

				def ask_stay_tavern player
					stay = nil
					while stay.nil?
						puts 'Do you want to stay at the tavern after work? [y/n]'
						chosen = $stdin.readline.strip
						if /^y|n$/ =~ chosen
							stay = chosen == 'y'
						end
					end
					stay
				end

			end

		end
	end
end
