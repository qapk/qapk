# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Move < ActionBase

				def ask_bottle player
					puts 'The bottle is turning!'
					initial_speed = (rand * 2 + 3) * 360
					initial_angle = 0.to_f
					[initial_speed, initial_angle]
				end

				def bottle_angle player, angle
					puts "The bottle stopped with an angle of #{angle}°."
				end

				def bottle_first_move player
					puts "This is your first move. The bottle will turn to compute your direction."
				end

				def bottle_north player
					puts "The dice for the direction of the north is 6. The bottle will turn to compute the direction of the north."
				end

				def square_nb player, square_nb
					puts "The dice for the number of squares to move is #{square_nb}."
				end

				def direction player, direction
					puts "The dice for the move direction is #{direction}."
					if QAPK::Action::Move::DIRECTION.has_key? direction
						puts "The direction of the move is to the #{QAPK::Action::Move::DIRECTION[direction]}."
					else
						puts "Special rules will apply."
					end
				end

				def north player, north
					puts "The dice for direction of the north is #{north}."
					if QAPK::Action::Move::NORTH.has_key? north
						print 'The north is '
						case QAPK::Action::Move::NORTH[north]
						when :front
							puts 'in front of you.'
						when :behind
							puts 'behind you.'
						when :left, :right
							puts "to your #{QAPK::Action::Move::NORTH[north]}."
						end
					else
						puts "Special rules will apply."
					end
				end

				def player_direction player, player_direction
					puts "You are facing to the #{player_direction} of the board."
				end

				def dice_is_one player
					puts "Special rules must apply because a dice is 1."
				end

				def using_saved_dice player, value
					puts "The dice has already been rolled. Using its value: #{value}."
				end

				def dice_rerolled player, value
					puts "The dice has been re-rolled: #{value}."
				end

				def using_previous_dice player, value
					puts "The dice is 6! Using the dice from the preceding step: #{value}."
				end

				def dice_is_one_no_previous_dice player
					puts "The dice is 6! Since this is the first step, the dice for the next step will be rolled and remembered."
				end

				def saved_rolls player, nb_roll
					puts "The dice has been rolled for the next #{nb_roll} steps."
				end

				def dice_is_one_move_over player
					puts "Dice is 1 again! Your move is over."
				end

				def dice_is_six_move_over player
					puts "The dice has not been anything but 6 in your remaining steps. Your move is other."
				end

				def move_towards_graal player
					puts "The dice for direction is 6. You're moving towards the Grail."
				end

			end

		end
	end
end
