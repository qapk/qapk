# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class GoTo < ActionBase

				def ask_place player
					place_chosen = nil
					while place_chosen.nil?
						puts 'Where do you want to go?'
						model.places.keys.each_with_index do |place, index|
							index += 1 # Index starts at 0 but we need it to start at 1
							print "#{index}. "
							place.view.print_name
							print "\n"
						end
						chosen = $stdin.readline.strip
						if /^\d+$/ =~ chosen
							chosen = chosen.to_i - 1
							place_chosen = model.places.keys.at chosen
						end
					end
					place_chosen
				end

			end

		end
	end
end
