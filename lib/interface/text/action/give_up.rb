# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class GiveUp < ActionBase

				def ask_essay player
					puts 'To be allowed to give up, you need to write an essay about the reasons that make you think this game is too long, or that you have more interesting hobbies, your relationship with games, and with this game in particular, your reflexions about life, the universe, and everything. Finish your essay by typing Ctrl-D'
					essay = ''
					until $stdin.eof?
						essay += $stdin.read
					end
					essay
				end

			end

		end
	end
end
