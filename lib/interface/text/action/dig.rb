# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Dig < ActionBase

					def ask_squares_to_dig player, squares, nb_squares, distance_max
						puts "You need to choose which squares you want to dig. The first #{squares.size} squares around you have automatically been selected because you are required to dig them. You cannot choose a square that is at a distance greater than #{distance_max} from you."

						squares_selected = []
						while nb_squares > 0
							puts "You have #{nb_squares} squares to select left. Which squares do you want to dig?"
							answer = $stdin.readline.strip
							if answer.downcase == "none"
								nb_squares = 0
							else
								m = /(\d+)(-(\d+))?,?\s*(\d+)(-(\d))?/.match answer
								if m.nil?
									puts "Invalid answer."
								else
									r1 = m[1].to_i
									r2 = ((not m[3].nil?) and m[3] or r1).to_i
									c1 = m[4].to_i
									c2 = ((not m[6].nil?) and m[6] or c1).to_i
									squares_candidate = []
									r1.upto r2 do |row|
										c1.upto c2 do |column|
											squares_candidate.push [row, column] if (squares_selected.index [row, column]).nil? and (squares.index [row, column]).nil?
										end
									end
									squares_selected += squares_candidate
									nb_squares -= squares_candidate.size
								end
							end
						end
						squares_selected
					end

					def too_many_squares player, nb_squares, nb_squares_selected
						puts "You selected too much squares: #{nb_squares_selected} instead of #{nb_squares}."
					end

					def squares_too_far player, distance_max, not_ok_squares
						puts "You cannot select the following squares because they are at a distance greater than #{distance_max} from you:"
						not_ok_squares.each do |square|
							puts "#{square[0]}, #{square[1]}"
						end
					end

					def print_onions_found player, nb_onions
						puts "You found #{nb_onions} onions while digging."
					end

					def print_graal_found player, graal_index
						puts "You found the Grail at #{graal_index[0]}, #{graal_index[1]}!"
					end

			end

		end
	end
end
