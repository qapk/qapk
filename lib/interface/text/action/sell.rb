# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Sell < ActionBase

				def ask_slaves_to_sell player
					slaves_to_sell = nil
					while slaves_to_sell.nil?
						puts 'How many slaves do you want to sell?'
						slaves_to_sell = /^(\d+)$/.match $stdin.readline.strip do |m|
							m[1].to_i
						end
					end
					slaves_to_sell
				end

				def print_not_enough_slaves player, slaves_price
					puts "You do not have enough slaves to sell, thus you have to pay a penalty of #{slaves_price[0]} piece of bread and #{slaves_price[1]} onions."
				end

				def print_slaves_sold player, slaves_healthy, slaves_ill
					puts "You sold #{slaves_healthy} healthy slaves and #{slaves_ill} ill slaves."
				end

			end

		end
	end
end
