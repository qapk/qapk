# encoding: utf-8

require 'interface/text/action_base'

module QAPK
	module Interface
		module Action

			class Doublette < ActionBase

				def not_runable player
						puts 'Your opponent hasn\'t moved yet. You cannot choose this action.'
				end

			end

		end
	end
end
