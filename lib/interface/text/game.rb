# encoding: utf-8

require 'interface/object'
require 'interface/view'

module QAPK

	module Interface

		class Game < Object
			include View

			def action_ask player
				(view model.board).draw
				puts "#{player.name}, it's your turn!"
				puts 'Money:'
				puts "  Bread: #{player.money[0]}"
				puts "  Onion: #{player.money[1]}"
				puts 'Slaves:'
				puts "  Healthy: #{player.slaves[0]}"
				puts "  Ill: #{player.slaves[1]}"
				puts "Week day: #{player.week_day}"
				if player.silenced > 0
					puts "Your vow of silence ends in #{player.silenced} turns"
				elsif player.silenced == 0
					puts "Your vow of silence just ended"
				end
				action_chosen = nil
				begin
					while action_chosen.nil?
						puts 'Please choose your action:'
						model.actions.each_with_index do |action, index|
							index += 1 # Index starts at 0 but we need it to start at 1
							print "#{index}. "
							(view action).print_name
							print "\n"
						end
						chosen = $stdin.readline.strip
						if /^\d+$/ =~ chosen
							chosen = chosen.to_i - 1
							action_chosen = model.actions.at chosen
						end
					end
					action_chosen
				rescue EOFError
					nil
				end
			end

			def winner players, winner
				puts "#{winner.name} wins!"
			end

		end

	end
end
