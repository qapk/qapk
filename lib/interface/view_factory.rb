# encoding: utf-8

require 'misc'

module QAPK

	module Interface

		class ViewFactoryRulesBase

			def initialize interface, factory
				@interface = interface
				@factory = factory
			end

			def stop
			end

			def view_file model
				file = 'interface/' + @interface.to_s
				(model.game_class.name.split '::').each do |module_name|
					next if module_name == Module.nesting.last.name
					file += '/' + module_name.underscore
				end
				file
			end

			def view_class model
				klass = Interface
				(model.game_class.name.split '::').each do |module_name|
					next if module_name == Module.nesting.last.name
					klass = klass.const_get module_name
				end
				klass
			end

			def instanciate klass, model
				klass.new @factory, model
			end

		end

		class ViewFactory

			def self.create *args
				view_factory = self.new *args
				yield view_factory
				view_factory.stop
			end

			def initialize interface, *args
				@interface = interface
				@views = Hash.new
				load_rules *args
			end

			def stop
				@rules.stop
			end

			def has_view? model
				begin
					load_view model
					@rules.view_class model
					true
				rescue LoadError => e
					raise e if not e.message =~ /-- #{@rules.view_file model}$/
					false
				end
			end

			def view model
				if not @views.has_key? model
					load_view model
					klass = @rules.view_class model
					@views[model] = @rules.instanciate klass, model
				end
				@views[model]
			end

			protected

			def load_view model
				file = @rules.view_file model
				require file
			end

			def load_rules *args
				begin
					require 'interface/' + @interface.to_s + '/view_factory_rules'
					klass = ViewFactoryRules
				rescue LoadError
					klass = ViewFactoryRulesBase
				end
				@rules = klass.new @interface, self, *args
			end

		end

	end

end
