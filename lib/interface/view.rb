# encoding: utf-8

require 'interface/view_factory'

module QAPK

	module Interface

		module View

			attr_reader :model

			def initialize factory, model
				@factory = factory
				@model = model
			end

			def view model
				@factory.view model
			end

			def has_view? model
				@factory.has_view? model
			end

		end

	end

end
