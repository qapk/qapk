# encoding: utf-8

require 'interface/object'
require 'interface/view'

module QAPK

	module Interface

		class Remote < Object
			include View

			def initialize server, factory, model
				super factory, model
				@server = server
			end

			def method_missing name, players, *args
				@server.ask model, name, players, *args
			end

		end

	end

end
