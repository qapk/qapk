# encoding: utf-8

require 'drb/drb'
require 'monitor'

require 'misc'
require 'interface/object'
require 'interface/drb/remote'
require 'interface/view_factory'

module QAPK

	class Object
		include DRb::DRbUndumped
		extend DRb::DRbUndumped
	end

	module Interface

		class State < Object
			attr_reader :model, :name, :players, :args
			def initialize model, name, players, *args
				@model = model
				@name = name
				@players = if players.respond_to? :collect
				           	Hash[*players.collect do |player|
				           	     	[player.to_sym, player]
				           	     end.flatten]
				           else
				          	{players.to_sym => players}
				          end
				@args = DRb::DRbArray.new args
			end
		end

		class Server < Object

			attr_reader :clients, :result

			def initialize
				@clients = Array.new
				@clients.extend MonitorMixin
				@result = Hash.new
				@result.extend MonitorMixin
				@result_cv = @result.new_cond
				@state_mutex = Mutex.new
				@state_cv = ConditionVariable.new
				@clients_state = Hash.new
			end

			def register client
				@clients.synchronize do
					@clients.push client
					client.ask @state
				end
			end

			def unregister player
				@clients.synchronize do
					@clients.delete player
				end
			end

			def answer player, *results
				@result.synchronize do
					result = if results.size == 1
					         	results[0]
					         else
					         	results
					         end
					@result[player] = result
					@result_cv.signal
				end
			end

			def ask model, name, players, *args
				@result.synchronize do
					@result.clear
				end
				@state_mutex.synchronize do
					@state = State.new model, name, players, *args
					@state_cv.signal
				end
				@clients.synchronize do
					@clients.each do |client|
						client.ask @state
					end
				end
				@result.synchronize do
					@result_cv.wait_until do
						@result.size == @state.players.size
					end
					result = Hash[@result]
					result = result.values[0] if result.size == 1
					@state_mutex.synchronize do
					end
					result
				end
			end

			def state client = nil
				@state_mutex.synchronize do
					@state_cv.wait @state_mutex until @state != @clients_state[client] unless client.nil?
					@clients_state[client] = @state
					yield @state
				end
			end

		end

		class ViewFactoryRules < ViewFactoryRulesBase

			def initialize interface, factory, uri
				super interface, factory
				@server = Server.new
				@server_drb = DRb::DRbServer.new uri, @server
			end

			def stop
				@server_drb.stop_service
			end

			def view_file model
				'interface/drb/remote'
			end

			def view_class model
				Interface::Remote
			end

			def instanciate klass, model
				klass.new @server, @factory, model
			end

		end

	end

end
